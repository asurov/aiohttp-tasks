from aiohttp import web
from routes import setup_routes
from motor.motor_asyncio import AsyncIOMotorClient

client = AsyncIOMotorClient()
app = web.Application()
app.td = client['test_database']

setup_routes(app)
web.run_app(app)


from views import  create_task, create_user, delete_user, assigne_user, get_all_free_task, get_all_task_by_id

def setup_routes(app):
    app.router.add_post('/cu', create_user)
    app.router.add_post('/du', delete_user)
    app.router.add_post('/ct', create_task)
    app.router.add_post('/au', assigne_user)
    app.router.add_get('/aft', get_all_free_task)
    app.router.add_get('/{user}', get_all_task_by_id)


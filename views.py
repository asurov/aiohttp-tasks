from aiohttp import web

async def create_task(req):
    data = await req.post()
    db = req.app.td
    result = await db.tasks_collection.insert_one(dict(data))
    return web.Response(text="Ok")


async def create_user(req):
    data = await req.post()
    db = req.app.td
    result = await db.users_collection.insert_one(dict(data))
    return web.Response(text="User Created")


async def delete_user(req):
    data = await req.post()
    db = req.app.td
    result = await db.users_collection.delete_one(dict(data))
    return web.Response(text="User DELETED")


async def assigne_user(req):
    data = await req.post()
    db = req.app.td
    try:
        task = data['title']
        user = data['user']
    except KeyError:
        return web.Response(text="Error",status=404)
    result = await db.tasks_collection.update_one({'title': task}, {'$set': {'user': user}})
    return web.Response(text="Task %s assigned %s" % (task,user))


async def get_all_free_task(req):
    db = req.app.td
    res = {}
    async for document in db.tasks_collection.find({'assigne': {'$eq': ''}}):
        res.update(dict(document))
    return web.Response(text=repr(res))


async def get_all_task_by_id(req):
    user = req.match_info.get('user')
    print(user)
    db = req.app.td
    res = {}
    async for document in db.tasks_collection.find({'assigne': {'$eq': user}}):
        res.update(dict(document))
    return web.Response(text=repr(res))

